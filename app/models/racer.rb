class Racer
  include ActiveModel::Model
  
  attr_accessor :id, :number, :first_name, :last_name, :gender, :group, :secs

  def initialize(params={})
    @id=params[:_id].nil? ? params[:id] : params[:_id].to_s
    @number = params[:number].to_i
    @first_name = params[:first_name]
    @last_name = params[:last_name]
    @gender = params[:gender]
    @group = params[:group]
    @secs = params[:secs].to_i
  end

  def save
    result = self.class.collection
               .insert_one(
                 _id: @id,
                 number: @number,
                 group: @group,
                 secs: @secs,
                 gender: @gender,
                 first_name: @first_name,
                 last_name: @last_name)
    @id = result.inserted_id
  end

  def update(params)
    # object mutation
    @number  = params[:number].to_i  
    @first_name = params[:first_name]
    @last_name = params[:last_name]
    @group = params[:group] 
    @secs = params[:secs].to_i 
    @gender = params[:gender]

    # persistence:
    params.slice!(:number, :first_name, :last_name, :group, :gender, :secs)
    _id = BSON::ObjectId(@id)
    self.class.collection.find_one_and_update({_id: _id}, params)
  end

  def destroy
     self.class.collection.delete_one({_id:  BSON::ObjectId(@id)})
  end

  # ActiveModel support
  
  def persisted?
    !@id.nil?
  end

  def created_at
    nil
  end

  def updated_at
    nil
  end
  
  # Class Methods follow
  
  def self.mongo_client
    Mongoid::Clients.default
  end

  def self.collection
    self.mongo_client[:racers]
  end
  
  def self.all(prototype={}, sort={}, skip=0, limit=nil)
    self.collection.find(prototype, @options={ limit: limit, skip: skip, sort: sort })
  end
  
  def self.find id
    _id = id.is_a?(String) ?   BSON::ObjectId(id) : id
    params = self.all({ _id: _id }).first
    if params
      Racer.new(params)      
    else
      params
    end
  end

  def self.paginate(params)
    page = (params[:page] || 1).to_i
    limit = (params[:per_page] || 30).to_i
    skip = (page - 1) * limit

    racers = []
    self.all({}).sort({'number': 1}).limit(limit).skip(skip).each do |doc|
      racers << Racer.new(doc)
    end

    total = self.all.count

    WillPaginate::Collection.create(page, limit, total) do |pager|
      pager.replace(racers)
    end
  end
end
