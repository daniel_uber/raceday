require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Raceday
  class Application < Rails::Application
    # bootstrap mongoid with connection details
    Mongoid.load!('./config/mongoid.yml')

    # which ORM are we using?
    config.generators { |g| g.orm :mongoid }
  end
end
